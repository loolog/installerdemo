﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO;

namespace InstallerDemo
{
    public class InstallerDataModel : INotifyPropertyChanged, IDataErrorInfo, IInstallerModel
    {
        private string _installPath;
        private string _srcPath;

        public InstallerDataModel()
        {
            _installPath = Path.Combine(Directory.GetCurrentDirectory(), "Destination");
            _srcPath = Path.Combine(Directory.GetCurrentDirectory(), "Tiedostot");
        }

        public string InstallPath
        {
            get { return _installPath; }
            set
            {
                _installPath = value;
                OnPropertyChanged("InstallPath");
            }
        }

        public string SourcePath
        {
            get { return _srcPath; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public string Error
        {
            get { return ""; }
        }

        public string this[string columnName]
        {
            get
            {
                string result = null;
                if (columnName == "InstallPath")
                {
                    if (string.IsNullOrWhiteSpace(_installPath))
                    {
                        result = "Please enter a folder path.";
                    }
                }

                return result;
            }
        }
    }
}