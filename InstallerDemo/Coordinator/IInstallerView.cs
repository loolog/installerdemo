﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public interface IInstallerView
    {
        void CloseView();

        IWelcomeView ShowWelcome();
        IOptionsView ShowOptions();
        IConfirmationView ShowConfirmation();
        IProgressView ShowProgress();
        ICompletedView ShowCompleted();
        IErrorView ShowError();

        event VoidHandler ExitRequested;
    }
}
