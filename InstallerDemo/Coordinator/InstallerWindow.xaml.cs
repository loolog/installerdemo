﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstallerDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class InstallerWindow : Window, IInstallerView
    {
        private bool _forceClose;
        private IInstallerModel _model;

        public InstallerWindow()
        {
            InitializeComponent();

            _forceClose = false;
            _model = new InstallerDataModel();

            IInstallerCoordinator coordinator = new InstallerCoordinator(this, _model);
        }

        public void CloseView()
        {
            _forceClose = true;
            this.Close();
        }

        public IWelcomeView ShowWelcome()
        {
            InstallerDemo.Views.Welcome.WelcomeView view = new Views.Welcome.WelcomeView();
            this.Content = view;

            return view;
        }

        public IOptionsView ShowOptions()
        {
            InstallerDemo.Views.Options.OptionsView view = new Views.Options.OptionsView(_model);
            this.Content = view;

            return view;
        }

        public IConfirmationView ShowConfirmation()
        {
            InstallerDemo.Views.Confirmation.ConfirmationView view = new Views.Confirmation.ConfirmationView(_model);
            this.Content = view;

            return view;
        }

        public IProgressView ShowProgress()
        {
            InstallerDemo.Views.Progress.ProgressView view = new Views.Progress.ProgressView();
            this.Content = view;

            return view;
        }

        public ICompletedView ShowCompleted()
        {
            InstallerDemo.Views.Completed.CompletedView view = new Views.Completed.CompletedView();
            this.Content = view;

            return view;
        }

        public IErrorView ShowError()
        {
            InstallerDemo.Views.Error.ErrorView view = new Views.Error.ErrorView();
            this.Content = view;

            return view;
        }

        public event VoidHandler ExitRequested;

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if ((ExitRequested != null) && (_forceClose == false))
            {
                ExitRequested();
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }
    }
}
