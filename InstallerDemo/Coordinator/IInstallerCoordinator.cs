﻿using System;
namespace InstallerDemo
{
    public interface IInstallerCoordinator
    {
        void NextView();
        void PreviousView();
        void ShowError(string message);
        void TryExit();
    }
}
