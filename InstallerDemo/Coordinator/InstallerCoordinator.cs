﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace InstallerDemo
{
    public class InstallerCoordinator : IInstallerCoordinator
    {
        private IInstallerView _view;
        private InstallerMode _currentMode;
        private IPresenter _currentPresenter;
        private IInstallerModel _model;

        public InstallerCoordinator(IInstallerView view, IInstallerModel model)
        {
            _view = view;
            _view.ExitRequested += this.TryExit;
            _model = model;

            if (Directory.Exists(_model.SourcePath))
            {
                ShowWelcome();
            }
            else
            {
                ShowError("Asennettavien tiedostojen kansiota ei ole.");
            }
        }

        public void TryExit()
        {
            if (_currentPresenter.CanShutdown())
            {
                _view.CloseView();
            }
        }

        public void NextView()
        {
            switch (_currentMode)
            {
                case InstallerMode.Welcome:
                    ShowOptions();
                    break;
                case InstallerMode.Options:
                    ShowConfirmation();
                    break;
                case InstallerMode.Confirmation:
                    ShowProgress();
                    break;
                case InstallerMode.Progress:
                    ShowCompleted();
                    break;
                default:
                    // For the other modes a next view is not defined
                    break;
            }
        }

        public void PreviousView()
        {
            switch (_currentMode)
            {
                case InstallerMode.Options:
                    ShowWelcome();
                    break;
                case InstallerMode.Confirmation:
                    ShowOptions();
                    break;
                default:
                    // For the other cases going backwards is not possible
                    break;
            }
        }

        public void ShowError(string message)
        {
            _currentMode = InstallerMode.Error;
            _currentPresenter = new ErrorPresenter(_view.ShowError(), _model, this, message);
        }

        private void ShowWelcome()
        {
            _currentMode = InstallerMode.Welcome;
            _currentPresenter = new WelcomePresenter(_view.ShowWelcome(), _model, this);
        }

        private void ShowOptions()
        {
            _currentMode = InstallerMode.Options;
            _currentPresenter = new OptionsPresenter(_view.ShowOptions(), _model, this);
        }

        private void ShowConfirmation()
        {
            _currentMode = InstallerMode.Confirmation;
            _currentPresenter = new ConfirmationPresenter(_view.ShowConfirmation(), _model, this);
        }

        private void ShowProgress()
        {
            _currentMode = InstallerMode.Progress;
            _currentPresenter = new ProgressPresenter(_view.ShowProgress(), _model, this);
        }

        private void ShowCompleted()
        {
            _currentMode = InstallerMode.Completed;
            _currentPresenter = new CompletedPresenter(_view.ShowCompleted(), _model, this);
        }
    }
}
