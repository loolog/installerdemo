﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public interface IOptionsView
    {
        event VoidHandler CancelRequested;
        event VoidHandler NextViewRequested;
        event VoidHandler PreviousViewRequested;

        void AllowNext(bool allow);
    }
}
