﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public class OptionsPresenter : IPresenter
    {
        private IOptionsView _view;
        private IInstallerModel _model;
        private IInstallerCoordinator _coordinator;

        public OptionsPresenter(IOptionsView view, IInstallerModel model, IInstallerCoordinator coordinator)
        {
            _view = view;
            _view.CancelRequested += this._view_CancelRequested;
            _view.NextViewRequested += this._view_NextViewRequested;
            _view.PreviousViewRequested += this._view_PreviousViewRequested;

            _model = model;
            _model.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(_model_PropertyChanged);

            _coordinator = coordinator;
        }

        void _model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "InstallPath")
            {
                if (_model["InstallPath"] == null)
                {
                    _view.AllowNext(true);
                }
                else
                {
                    _view.AllowNext(false);
                }
            }
        }

        public void _view_CancelRequested()
        {
            _coordinator.TryExit();
        }

        public void _view_NextViewRequested()
        {
            if (_model["InstallPath"] == null)
            {
                _coordinator.NextView();
            }
        }

        public void _view_PreviousViewRequested()
        {
            _coordinator.PreviousView();
        }

        public bool CanShutdown()
        {
            return true;
        }
    }
}
