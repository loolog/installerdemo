﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstallerDemo.Views.Options
{
    /// <summary>
    /// Interaction logic for OptionsView.xaml
    /// </summary>
    public partial class OptionsView : UserControl, IOptionsView
    {
        IInstallerModel _model;

        public OptionsView()
        {
            InitializeComponent();
        }
        public OptionsView(IInstallerModel model) : this()
        {
            _model = model;
            this.DataContext = _model;
        }

        public IInstallerModel Model
        {
            get { return _model; }
            set 
            { 
                _model = value; 
                this.DataContext = _model; 
            }
        }
        public event VoidHandler CancelRequested;

        public event VoidHandler NextViewRequested;

        public event VoidHandler PreviousViewRequested;

        public void AllowNext(bool allow)
        {
            this.nextButton.IsEnabled = allow;
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            if (NextViewRequested != null)
                NextViewRequested();
        }

        private void previousButton_Click(object sender, RoutedEventArgs e)
        {
            if (PreviousViewRequested != null)
                PreviousViewRequested();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (CancelRequested != null)
                CancelRequested();
        }

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.ShowNewFolderButton = true;

            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
                _model.InstallPath = dialog.SelectedPath;
        }
    }
}
