﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public class ErrorPresenter : IPresenter
    {
        private IErrorView _view;
        private IInstallerCoordinator _coordinator;

        public ErrorPresenter(IErrorView view, IInstallerModel model, IInstallerCoordinator coordinator, string message)
        {
            _view = view;
            _view.ExitRequested += this._view_ExitRequested;
            _view.SetErrorMessage(message);

            _coordinator = coordinator;
        }

        public void _view_ExitRequested()
        {
            _coordinator.TryExit();
        }

        public bool CanShutdown()
        {
            return true;
        }
    }
}
