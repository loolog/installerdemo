﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public interface IErrorView
    {
        event VoidHandler ExitRequested;

        void SetErrorMessage(string message);
    }
}
