﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstallerDemo.Views.Error
{
    /// <summary>
    /// Interaction logic for ErrorView.xaml
    /// </summary>
    public partial class ErrorView : UserControl, IErrorView 
    {
        public ErrorView()
        {
            InitializeComponent();
        }

        public event VoidHandler ExitRequested;

        public void SetErrorMessage(string message)
        {
            this.messageBlock.Text = message;
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            if (ExitRequested != null)
                ExitRequested();
        }
    }
}
