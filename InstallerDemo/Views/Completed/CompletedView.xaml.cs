﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstallerDemo.Views.Completed
{
    /// <summary>
    /// Interaction logic for CompletedView.xaml
    /// </summary>
    public partial class CompletedView : UserControl, ICompletedView
    {
        public CompletedView()
        {
            InitializeComponent();
        }

        public event VoidHandler FinishRequested;

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            if (FinishRequested != null)
                FinishRequested();
        }
    }
}
