﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public class CompletedPresenter : IPresenter
    {
        private ICompletedView _view;
        private IInstallerCoordinator _coordinator;

        public CompletedPresenter(ICompletedView view, IInstallerModel model, IInstallerCoordinator coordinator)
        {
            _view = view;
            _view.FinishRequested += _view_FinishRequested;
            _coordinator = coordinator;
        }

        public void _view_FinishRequested()
        {
            _coordinator.TryExit();
        }

        public bool CanShutdown()
        {
            return true;
        }
    }
}
