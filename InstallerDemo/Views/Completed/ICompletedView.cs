﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public interface ICompletedView
    {
        event VoidHandler FinishRequested;
    }
}
