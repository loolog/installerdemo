﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstallerDemo.Views.Welcome
{
    /// <summary>
    /// Interaction logic for WelcomeView.xaml
    /// </summary>
    public partial class WelcomeView : UserControl, IWelcomeView
    {
        public event VoidHandler CancelRequested;

        public event VoidHandler NextViewRequested;

        public WelcomeView()
        {
            InitializeComponent();
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            if (NextViewRequested != null)
                NextViewRequested();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (CancelRequested != null)
                CancelRequested();
        }
    }
}
