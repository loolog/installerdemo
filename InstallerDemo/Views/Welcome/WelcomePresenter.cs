﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    class WelcomePresenter : IPresenter
    {
        IWelcomeView _view;
        IInstallerCoordinator _coordinator;

        public WelcomePresenter(IWelcomeView view, IInstallerModel model, IInstallerCoordinator coordinator)
        {
            _view = view;
            _coordinator = coordinator;

            _view.CancelRequested += this._view_CancelRequested;
            _view.NextViewRequested += this._view_NextViewRequested;
        }

        public void _view_CancelRequested()
        {
            _coordinator.TryExit();
        }

        public void _view_NextViewRequested()
        {
            _coordinator.NextView();
        }

        public bool CanShutdown()
        {
            return true;
        }
    }
}
