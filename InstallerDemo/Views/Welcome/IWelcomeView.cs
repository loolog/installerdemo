﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public interface IWelcomeView
    {
        event VoidHandler CancelRequested;
        event VoidHandler NextViewRequested;
    }
}
