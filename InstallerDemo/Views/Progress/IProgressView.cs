﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public interface IProgressView
    {
        event VoidHandler CancelRequested;

        void SetInstallProgress(int progress);

        bool ConfirmCancel();
    }
}
