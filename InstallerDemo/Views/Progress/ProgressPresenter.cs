﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO;

namespace InstallerDemo
{
    public class ProgressPresenter : IPresenter
    {
        private IProgressView _view;
        private IInstallerModel _model;
        private IInstallerCoordinator _coordinator;
        private BackgroundWorker _worker;

        public ProgressPresenter(IProgressView view, IInstallerModel model, IInstallerCoordinator coordinator)
        {
            _view = view;
            _view.CancelRequested += this.TryCancel;
            _view.SetInstallProgress(0);

            _model = model;
            _coordinator = coordinator;

            _worker = new BackgroundWorker();
            _worker.WorkerReportsProgress = true;
            _worker.WorkerSupportsCancellation = true;
            _worker.DoWork += new DoWorkEventHandler(_worker_DoWork);
            _worker.ProgressChanged += new ProgressChangedEventHandler(_worker_ProgressChanged);
            _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_worker_RunWorkerCompleted);

            _worker.RunWorkerAsync();
        }

        public void _worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                _coordinator.TryExit();
            }
            else if (e.Error != null)
            {
                _coordinator.ShowError(e.Error.Message);
            }
            else
            {
                _coordinator.NextView();
            }
        }

        public void _worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _view.SetInstallProgress(e.ProgressPercentage);
        }

        public void _worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            int filecount = Directory.EnumerateFiles(_model.SourcePath, "*", SearchOption.AllDirectories).Count();

            DirectoryInfo sourceDI = new DirectoryInfo(_model.SourcePath);
            DirectoryInfo destDI = new DirectoryInfo(_model.InstallPath);

            Copy(sourceDI, destDI, worker, e, filecount, 0);
        }

        private int Copy(DirectoryInfo sourceDI, DirectoryInfo destDI, BackgroundWorker worker, DoWorkEventArgs e, int filecount, int copiedfiles)
        {
            if (worker.CancellationPending)
            {
                e.Cancel = true;
                return copiedfiles;
            }
            if (Directory.Exists(destDI.FullName) == false)
            {
                Directory.CreateDirectory(destDI.FullName);
            }

            foreach (FileInfo fi in sourceDI.GetFiles())
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return copiedfiles;
                }
                
                fi.CopyTo(Path.Combine(destDI.ToString(), fi.Name), true);
                copiedfiles++;
                double progress = (double)copiedfiles/(double)filecount*100d;
                worker.ReportProgress((int)progress);
            }

            foreach (DirectoryInfo sourceSubDI in sourceDI.GetDirectories())
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return copiedfiles;
                }

                DirectoryInfo destSubDI = destDI.CreateSubdirectory(sourceSubDI.Name);
                copiedfiles = Copy(sourceSubDI, destSubDI, worker, e, filecount, copiedfiles);
            }
            return copiedfiles;
        }

        public bool CanShutdown()
        {
            if (_worker.IsBusy)
            {
                TryCancel();

                // don't allow shutdown just yet, retry after worker is cancelled
                return false;
            }
            else
            {
                return true;
            }
        }

        public void TryCancel()
        {
            bool doshutdown = _view.ConfirmCancel();
            if (doshutdown)
            {
                _worker.CancelAsync();
            }
        }
    }
}
