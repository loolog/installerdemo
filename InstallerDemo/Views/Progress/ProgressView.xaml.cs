﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstallerDemo.Views.Progress
{
    /// <summary>
    /// Interaction logic for ProgressView.xaml
    /// </summary>
    public partial class ProgressView : UserControl, IProgressView
    {
        public ProgressView()
        {
            InitializeComponent();
        }

        public event VoidHandler CancelRequested;

        public void SetInstallProgress(int progress)
        {
            this.progressBar.Value = progress;
            if (progress == 0)
                this.progressBar.IsIndeterminate = true;
            else
                this.progressBar.IsIndeterminate = false;
        }

        public bool ConfirmCancel()
        {
            MessageBoxResult result = MessageBox.Show("Haluatko varmasti keskeyttää asennuksen?", "Keskeytä?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                this.textBlock1.Text = "Asennus keskeytetään kun keskeneräinen kopiointi päättyy.";
                this.progressBar.IsIndeterminate = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (CancelRequested != null)
                CancelRequested();
        }
    }
}
