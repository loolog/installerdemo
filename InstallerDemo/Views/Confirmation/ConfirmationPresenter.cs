﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public class ConfirmationPresenter : IPresenter
    {
        private IConfirmationView _view;
        private IInstallerCoordinator _coordinator;

        public ConfirmationPresenter(IConfirmationView view, IInstallerModel model, IInstallerCoordinator coordinator)
        {
            _view = view;
            _view.CancelRequested += this._view_CancelRequested;
            _view.NextViewRequested += this._view_NextViewRequested;
            _view.PreviousViewRequested += this._view_PreviousViewRequested;

            _coordinator = coordinator;
        }

        public void _view_CancelRequested()
        {
            _coordinator.TryExit();
        }

        public void _view_NextViewRequested()
        {
            _coordinator.NextView();
        }

        public void _view_PreviousViewRequested()
        {
            _coordinator.PreviousView();
        }

        public bool CanShutdown()
        {
            return true;
        }
    }
}
