﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstallerDemo
{
    public interface IConfirmationView
    {
        event VoidHandler CancelRequested;
        event VoidHandler NextViewRequested;
        event VoidHandler PreviousViewRequested;
    }
}
