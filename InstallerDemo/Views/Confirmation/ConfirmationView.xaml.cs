﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstallerDemo.Views.Confirmation
{
    /// <summary>
    /// Interaction logic for ConfirmationView.xaml
    /// </summary>
    public partial class ConfirmationView : UserControl, IConfirmationView
    {
        private IInstallerModel _model;

        public ConfirmationView()
        {
            InitializeComponent();
        }

        public ConfirmationView(IInstallerModel model) : this()
        {
            _model = model;
            this.DataContext = _model;
        }

        public event VoidHandler CancelRequested;

        public event VoidHandler NextViewRequested;

        public event VoidHandler PreviousViewRequested;

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            if (NextViewRequested != null)
                NextViewRequested();
        }

        private void previousButton_Click(object sender, RoutedEventArgs e)
        {
            if (PreviousViewRequested != null)
                PreviousViewRequested();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (CancelRequested != null)
                CancelRequested();
        }
    }
}
