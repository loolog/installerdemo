﻿using System;
using System.ComponentModel;
namespace InstallerDemo
{
    public interface IInstallerModel : INotifyPropertyChanged, IDataErrorInfo
    {
        string InstallPath { get; set; }
        string SourcePath { get; }
    }
}
